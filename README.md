# Tinkercad
A Minetest mod. Adds nodes with the colours used by Tinkercad. This mod makes 36 nodes available, no texture, just plain colour.

## Description
This mod just adds 36 colourful nodes to your game in Minetest. The colours are chosen to be the same as Tinkercad uses. You may be aware that you can use Tinkercad to export your 3D model to a .schematic file, ready to use in Minecraft worlds. Also, you can convert this file into a .mts file with a tool like MTSEdit by bzt@gitlab and import it to your minetest world. With Tinkercad mod and the blocks.csv provided in this project you can import your 3D model from Tinkercad to your minetest world with the exact colour you used when you created it. The nodes are easily replaceable, to speed up the process of building and creating your structures.

## Usage
Preparation:
1. Download MTS Editor and follow the instructions to make it work properly.
2. Replace blocks.csv in \data folder with the file provided in this project.
3. It is recommended activating rollback in your world.

Usage:
1. Add Tinkercad mod to your world.
2. Create your model with Tinkercad and export it as .schematic.
3. Use MTS Editor to convert it to .mts file. Choose Mapping: mineclone2.
4. Place the .mts file into the \schem folder in your world.
5. Import it to your world. You will need a mod for this. The mod handle_schematics by Sokomine is strongly recommended, since you can easily replace nodes before putting them into the world.

## Support
@citaconrama in minetest forum.

## Author
@citaconrama

## Acknowledgment

bzt, who made MTSEdit, the amazing tool that easily translates Minecraft .schematic to Minetest .mts files.

https://forum.minetest.net/viewtopic.php?t=23724

Sokomine, who made the mod handle_schematics, which makes your life as a builder much easier.

https://content.minetest.net/packages/Sokomine/handle_schematics/

rubenwardy, who wrote Minetest Modding Book.

https://rubenwardy.com/minetest_modding_book/en/index.html.

And all the community and developers of Minetest.

## License
See license file for details.