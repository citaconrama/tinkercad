local S = minetest.get_translator("tinkercad")

minetest.register_node(minetest.get_current_modname()..":red", {
    description = S("Red"),
    tiles = {"^[colorize:#e91d2d"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_red", {
    description = S("Light Red"),
    tiles = {"^[colorize:#e9968c"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_red", {
    description = S("Dark Red"),
    tiles = {"^[colorize:#951a21"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":orange", {
    description = S("Orange"),
    tiles = {"^[colorize:#f5831f"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_orange", {
    description = S("Light orange"),
    tiles = {"^[colorize:#fbc59a"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_orange", {
    description = S("Dark orange"),
    tiles = {"^[colorize:#e35b22"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":yellow", {
    description = S("Yellow"),
    tiles = {"^[colorize:#ffdd1a"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_yellow", {
    description = S("Light yellow"),
    tiles = {"^[colorize:#f8e6b7"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_yellow", {
    description = S("Dark yellow"),
    tiles = {"^[colorize:#e1ad34"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":green", {
    description = S("Green"),
    tiles = {"^[colorize:#46b749"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_green", {
    description = S("Light green"),
    tiles = {"^[colorize:#c8e4bd"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_green", {
    description = S("Dark green"),
    tiles = {"^[colorize:#126936"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":teal", {
    description = S("Teal"),
    tiles = {"^[colorize:#75cedb"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_teal", {
    description = S("Light teal"),
    tiles = {"^[colorize:#b0e8ef"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_teal", {
    description = S("Dark teal"),
    tiles = {"^[colorize:#1c505a"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":sky_blue", {
    description = S("Sky blue"),
    tiles = {"^[colorize:#009fd7"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_sky_blue", {
    description = S("Light sky blue"),
    tiles = {"^[colorize:#85cde6"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_sky_blue", {
    description = S("Dark sky blue"),
    tiles = {"^[colorize:#0076a9"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":blue", {
    description = S("Blue"),
    tiles = {"^[colorize:#009fd7"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_blue", {
    description = S("Light blue"),
    tiles = {"^[colorize:#aebeed"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_blue", {
    description = S("Dark blue"),
    tiles = {"^[colorize:#192e62"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":purple", {
    description = S("Purple"),
    tiles = {"^[colorize:#7e3f98"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_purple", {
    description = S("Light purple"),
    tiles = {"^[colorize:#d3bfe5"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_purple", {
    description = S("Dark purple"),
    tiles = {"^[colorize:#492e72"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":pink", {
    description = S("Pink"),
    tiles = {"^[colorize:#d70b8c"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_pink", {
    description = S("Light pink"),
    tiles = {"^[colorize:#efb1d4"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_pink", {
    description = S("Dark pink"),
    tiles = {"^[colorize:#901c53"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":brown", {
    description = S("Brown"),
    tiles = {"^[colorize:#a97b50"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_brown", {
    description = S("Light brown"),
    tiles = {"^[colorize:#e2c095"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_brown", {
    description = S("Dark brown"),
    tiles = {"^[colorize:#603913"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":white", {
    description = S("White"),
    tiles = {"^[colorize:#ffffff"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":very_light_gray", {
    description = S("Very light gray"),
    tiles = {"^[colorize:#dddddd"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":light_gray", {
    description = S("Light gray"),
    tiles = {"^[colorize:#aaaaaa"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":gray", {
    description = S("Gray"),
    tiles = {"^[colorize:#666666"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":dark_gray", {
    description = S("Dark gray"),
    tiles = {"^[colorize:#333333"},
    buildable_to=true,
    groups={crumbly=1},
})
minetest.register_node(minetest.get_current_modname()..":black", {
    description = S("Black"),
    tiles = {"^[colorize:#000000"},
    buildable_to=true,
    groups={crumbly=1},
})